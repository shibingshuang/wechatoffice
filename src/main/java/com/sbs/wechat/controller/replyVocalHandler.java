package com.sbs.wechat.controller;

import com.soecode.wxtools.api.IService;
import com.soecode.wxtools.api.WxMessageHandler;
import com.soecode.wxtools.bean.WxXmlMessage;
import com.soecode.wxtools.bean.WxXmlOutMessage;
import com.soecode.wxtools.bean.msgbuilder.VoiceBuilder;
import com.soecode.wxtools.exception.WxErrorException;

import java.util.Map;

/**
 * Created by Shibs on 2020/6/15.
 */
public class replyVocalHandler implements WxMessageHandler {

    @Override
    public WxXmlOutMessage handle(WxXmlMessage wxXmlMessage, Map<String, Object> map, IService iService) throws WxErrorException {

        WxXmlOutMessage xmlOutMsg = WxXmlOutMessage.VOICE().mediaId("c7aygghWg7vZW0x8U2CNGHEUcRgQNncbeRYXZM0t6zM").toUser(wxXmlMessage.getFromUserName()).fromUser(wxXmlMessage.getToUserName()).build();
        return xmlOutMsg;
    }
}
