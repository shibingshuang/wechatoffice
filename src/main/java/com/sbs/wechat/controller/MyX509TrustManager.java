package com.sbs.wechat.controller;

import javax.net.ssl.X509TrustManager;

/**
 * Created by Shibs on 2020/6/15.
 * * 信任管理器
 */


public class MyX509TrustManager implements X509TrustManager {


    // 检查客户端证书
    @Override
    public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {

    }
    // 检查服务器端证书
    @Override
    public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {

    }

    // 返回受信任的X509证书数组
    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
        return null;
    }
}


