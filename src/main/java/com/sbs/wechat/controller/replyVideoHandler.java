package com.sbs.wechat.controller;

import com.soecode.wxtools.api.IService;
import com.soecode.wxtools.api.WxMessageHandler;
import com.soecode.wxtools.bean.WxXmlMessage;
import com.soecode.wxtools.bean.WxXmlOutMessage;
import com.soecode.wxtools.exception.WxErrorException;

import java.util.Map;

/**
 * Created by Shibs on 2020/6/15.
 */
public class replyVideoHandler implements WxMessageHandler {

    @Override
    public WxXmlOutMessage handle(WxXmlMessage wxXmlMessage, Map<String, Object> map, IService iService) throws WxErrorException {

        WxXmlOutMessage xmlOutMsg = WxXmlOutMessage.VIDEO().mediaId("c7aygghWg7vZW0x8U2CNGCSsPdDqvdsJ-Sgqmd39GNU").title("狗狗").description("它叫啥来着？").toUser(wxXmlMessage.getFromUserName()).fromUser(wxXmlMessage.getToUserName()).build();
        return xmlOutMsg;
    }
}
