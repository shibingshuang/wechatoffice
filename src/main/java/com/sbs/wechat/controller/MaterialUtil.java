package com.sbs.wechat.controller;

import com.soecode.wxtools.api.IService;
import com.soecode.wxtools.api.WxConsts;
import com.soecode.wxtools.api.WxService;
import com.soecode.wxtools.bean.WxVideoIntroduction;
import com.soecode.wxtools.bean.result.WxMediaUploadResult;
import com.soecode.wxtools.exception.WxErrorException;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import java.io.*;
import java.net.ConnectException;
import java.net.MulticastSocket;
import java.net.URL;

/**
 * Created by Shibs on 2020/6/19.
 */
@Component
public class MaterialUtil {
    private IService iService = new WxService();

    public String uploadMaterial(String type){
        WxMediaUploadResult result = new WxMediaUploadResult();
        try {
            if(WxConsts.MEDIA_VOICE.equals(type)){
                //这里注意，如果是上传非视频格式的素材，第三个参数(WxVideoIntroduction)为null即可
                //c7aygghWg7vZW0x8U2CNGHEUcRgQNncbeRYXZM0t6zM
                result = iService.uploadMedia(WxConsts.MEDIA_VOICE, new File("C://Users/Administrator/Desktop/sound_4.mp3"), null);
            }else if(WxConsts.MEDIA_VIDEO.equals(type)){
                //如果是上传视频Video，可以添加描述
                WxVideoIntroduction intro = new WxVideoIntroduction();
                intro.setTitle("狗狗");
                intro.setIntroduction("它叫啥来着？");
                result = iService.uploadMedia(WxConsts.MEDIA_VIDEO, new File("C://Users/Administrator/Desktop/video.mp4"), intro);
            }

        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return result.getMedia_id();

    }

    public static void main(String[] args){
        MaterialUtil m = new MaterialUtil();
        String media_id = m.uploadMaterial("video");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<-----"+media_id);
    }

}


