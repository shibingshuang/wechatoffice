package com.sbs.wechat.controller;

import com.soecode.wxtools.api.IService;
import com.soecode.wxtools.api.WxMessageRouter;
import com.soecode.wxtools.api.WxService;
import com.soecode.wxtools.bean.WxXmlMessage;
import com.soecode.wxtools.bean.WxXmlOutMessage;
import com.soecode.wxtools.util.xml.XStreamTransformer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Shibs on 2020/6/10.
 */
@RestController
public class WxController {

    private IService iService = new WxService();

    //对接微信服务器，验证服务器的有效性
    @RequestMapping(value="/wx",method = RequestMethod.GET)
    public String check(String signature, String timestamp, String nonce, String echostr) {
        if (iService.checkSignature(signature, timestamp, nonce, echostr)) {
            return echostr;
        }
        return null;
    }

    //接收微信服务器发来的消息
    @RequestMapping(value="/wx",method = RequestMethod.POST)
    public void handle(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        // 创建一个路由器
        WxMessageRouter router = new WxMessageRouter(iService);


        try {
            // 微信服务器推送过来的是XML格式。
            WxXmlMessage wx = XStreamTransformer.fromXml(WxXmlMessage.class, request.getInputStream());
            System.out.println("消息：\n " + wx.toString());
            String event = wx.getEvent();
            if("subscribe".equals(event)){
                router.rule().handler(new replyHandler()).end();
                // 把消息传递给路由器进行处理
                WxXmlOutMessage xmlOutMsg = router.route(wx);
                if (xmlOutMsg != null)
                    out.print(xmlOutMsg.toXml());
            }else if(event == null){
                String msgType = wx.getMsgType();
                if("text".equals(msgType)){
                    String content = wx.getContent();
                    if("小记语音".equals(content)){
                        router.rule().handler(new replyVocalHandler()).end();
                        // 把消息传递给路由器进行处理
                        WxXmlOutMessage xmlOutMsg = router.route(wx);
                        if (xmlOutMsg != null)
                            out.print(xmlOutMsg.toXml());

                    }else if("小记视频".equals(content)){
                        router.rule().handler(new replyVideoHandler()).end();
                        // 把消息传递给路由器进行处理
                        WxXmlOutMessage xmlOutMsg = router.route(wx);
                        if (xmlOutMsg != null)
                            out.print(xmlOutMsg.toXml());

                    }
                    else{
                        router.rule().handler(new replyNoneHandler()).end();
                        // 把消息传递给路由器进行处理
                        WxXmlOutMessage xmlOutMsg = router.route(wx);
                        if (xmlOutMsg != null)
                            out.print(xmlOutMsg.toXml());

                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

}

