package com.sbs.wechat.controller;

import com.soecode.wxtools.api.IService;
import com.soecode.wxtools.api.WxMessageHandler;
import com.soecode.wxtools.bean.WxXmlMessage;
import com.soecode.wxtools.bean.WxXmlOutMessage;
import com.soecode.wxtools.exception.WxErrorException;

import java.util.Map;

/**
 * Created by Shibs on 2020/6/15.
 */
public class replyHandler implements WxMessageHandler {
    private long num = 0;

    @Override
    public WxXmlOutMessage handle(WxXmlMessage wxXmlMessage, Map<String, Object> map, IService iService) throws WxErrorException {
        num += 1;
        WxXmlOutMessage xmlOutMsg = WxXmlOutMessage.TEXT().content("欢迎光临，您是第"+num+"位小记粉丝~\n" +
                "<a href='http://www.weather.com.cn/weather/101010100.shtml'>点我查看天气</a>\n"
        +"回复-->【小记语音】试试看~\n"+
                "回复-->【小记视频】有惊喜哟~").toUser(wxXmlMessage.getFromUserName()).fromUser(wxXmlMessage.getToUserName()).build();
        return xmlOutMsg;
    }
}
