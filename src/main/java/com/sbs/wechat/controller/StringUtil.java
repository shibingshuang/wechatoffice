package com.sbs.wechat.controller;

/**
 * Created by Shibs on 2020/6/15.
 */
public class StringUtil {
    public static boolean isEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }

}
